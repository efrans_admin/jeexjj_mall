/****************************************************
 * Description: ServiceImpl for 商品表
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.ItemEntity;
import com.xjj.mall.dao.ItemDao;
import com.xjj.mall.service.ItemService;

@Service
public class ItemServiceImpl extends XjjServiceSupport<ItemEntity> implements ItemService {

	@Autowired
	private ItemDao itemDao;

	@Override
	public XjjDAO<ItemEntity> getDao() {
		
		return itemDao;
	}
}