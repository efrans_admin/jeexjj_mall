package com.xjj.mall.service;

import java.util.List;

import com.xjj.framework.service.XjjService;
import com.xjj.mall.entity.AddressEntity;

/**
 * @author zhanghejie
 */
public interface AddressService extends XjjService<AddressEntity>{

    /**
     * 获取地址
     * @param userId
     * @return
     */
    List<AddressEntity> getAddressList(Long userId);
   
}
