package com.xjj;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.xjj.mall.common.jedis.JedisClientPool;

import redis.clients.jedis.JedisPool;

@Configuration
public class ApplicationConfig extends WebMvcConfigurerAdapter implements CommandLineRunner{
    
    @Value("${redis.host}")
	private String host;
    
    @Value("${redis.port}")
	private int port;
    
    @Bean(name="jedisPool")
    public JedisPool getJedisPool() {
    	JedisPool  jedisPool = new JedisPool(host,port);
        return jedisPool;
    }
    
    @Bean(name="jedisClient")
    public JedisClientPool getJedisClientPool() {
    	JedisClientPool  jedisClient = new JedisClientPool();
    	jedisClient.setJedisPool(getJedisPool());
        return jedisClient;
    }
    
    /**
     * 启动后执行
     */
    @Order(value=1)
   	@Override
   	public void run(String... args) throws Exception {
    	System.out.println("==========启动后自动执行接口===============");
   	}
}