<#--
/****************************************************
 * Description: 用户表的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/user/save" id=tabId>
   <input type="hidden" name="id" value="${user.id}"/>
   
   <@formgroup title='用户名'>
	<input type="text" name="username" value="${user.username}" check-type="required">
   </@formgroup>
   <@formgroup title='密码 md5加密存储'>
	<input type="text" name="password" value="${user.password}" check-type="required">
   </@formgroup>
   <@formgroup title='注册手机号'>
	<input type="text" name="phone" value="${user.phone}" >
   </@formgroup>
   <@formgroup title='注册邮箱'>
	<input type="text" name="email" value="${user.email}" >
   </@formgroup>
   <@formgroup title='sex'>
	<input type="text" name="sex" value="${user.sex}" >
   </@formgroup>
   <@formgroup title='address'>
	<input type="text" name="address" value="${user.address}" >
   </@formgroup>
   <@formgroup title='state'>
	<input type="text" name="state" value="${user.state}" check-type="number">
   </@formgroup>
   <@formgroup title='description'>
	<input type="text" name="description" value="${user.description}" >
   </@formgroup>
   <@formgroup title='role_id'>
	<input type="text" name="roleId" value="${user.roleId}" check-type="number">
   </@formgroup>
   <@formgroup title='头像'>
	<input type="text" name="file" value="${user.file}" >
   </@formgroup>
   <@formgroup title='created'>
	<@date name="created" dateValue=user.created required="required" default=true/>
   </@formgroup>
   <@formgroup title='updated'>
	<@date name="updated" dateValue=user.updated required="required" default=true/>
   </@formgroup>
</@input>