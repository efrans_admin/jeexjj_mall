<#--
/****************************************************
 * Description: t_mall_base的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/base/save" id=tabId>
   <input type="hidden" name="id" value="${base.id}"/>
   
   <@formgroup title='web_name'>
	<input type="text" name="webName" value="${base.webName}" >
   </@formgroup>
   <@formgroup title='key_word'>
	<input type="text" name="keyWord" value="${base.keyWord}" >
   </@formgroup>
   <@formgroup title='description'>
	<input type="text" name="description" value="${base.description}" >
   </@formgroup>
   <@formgroup title='source_path'>
	<input type="text" name="sourcePath" value="${base.sourcePath}" >
   </@formgroup>
   <@formgroup title='upload_path'>
	<input type="text" name="uploadPath" value="${base.uploadPath}" >
   </@formgroup>
   <@formgroup title='copyright'>
	<input type="text" name="copyright" value="${base.copyright}" >
   </@formgroup>
   <@formgroup title='count_code'>
	<input type="text" name="countCode" value="${base.countCode}" >
   </@formgroup>
   <@formgroup title='has_log_notice'>
	<input type="text" name="hasLogNotice" value="${base.hasLogNotice}" check-type="number">
   </@formgroup>
   <@formgroup title='log_notice'>
	<input type="text" name="logNotice" value="${base.logNotice}" >
   </@formgroup>
   <@formgroup title='has_all_notice'>
	<input type="text" name="hasAllNotice" value="${base.hasAllNotice}" check-type="number">
   </@formgroup>
   <@formgroup title='all_notice'>
	<input type="text" name="allNotice" value="${base.allNotice}" >
   </@formgroup>
   <@formgroup title='notice'>
	<input type="text" name="notice" value="${base.notice}" >
   </@formgroup>
   <@formgroup title='update_log'>
	<input type="text" name="updateLog" value="${base.updateLog}" >
   </@formgroup>
   <@formgroup title='front_url'>
	<input type="text" name="frontUrl" value="${base.frontUrl}" >
   </@formgroup>
</@input>